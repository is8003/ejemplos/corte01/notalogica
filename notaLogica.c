#include <stdio.h>

int main(void){

	float n0, n1, n2, n3, n4;						// Declaro variables n0, n1, n2, n3, n4 de tipo flotante.

	printf("Ingresa 5 notas (separadas por espacios): ");			// Pregunto al usuario 5 notas.
	scanf("%f %f %f %f %f", &n0, &n1, &n2, &n3, &n4);			// Obtengo y asigno valor de 5 notas.

	float prom = (n0 + n1 + n2 + n3 + n4) / 5;				// Declaro variable flotante prom y asigno promedio de 5 notas.
	(prom >= 3.0 && prom <= 5.0) ? puts("¡Pasaste!") : puts("No pasaste.");	// Si prom >= 3.0 && prom <= 5.0, entonces imprimo pasó asignatura, de lo contrario imprimo no pasó.

	return 0;
}
